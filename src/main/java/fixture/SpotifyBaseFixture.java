package fixture;

import static utils.Keys.ALBUM;
import static utils.Keys.ALBUM_DETAILS;
import static utils.Keys.ALBUM_RESPONSE;
import static utils.Keys.ARTIST;
import static utils.Keys.ARTISTS_DETAILS;
import static utils.Keys.ARTIST_RESPONSE;
import static utils.Keys.AUTHORIZATION;
import static utils.Keys.AUTHORIZATION_RESPONSE;
import static utils.dependencyInjection.Injector.instantiate;

import client.AlbumsClient;
import client.ArtistsClient;
import client.AuthorizationClient;
import io.restassured.response.Response;
import java.util.List;
import model.albums.Album;
import model.artists.Artist;
import model.authorization.AuthorizationResponse;
import utils.dependencyInjection.annotation.Inject;
import utils.store.StoreAwareContext;

public class SpotifyBaseFixture extends StoreAwareContext {

	@Inject
	protected AuthorizationClient authorizationClient;
	@Inject
	protected AlbumsClient albumsClient;
	@Inject
	protected ArtistsClient artistsClient;

	public AuthorizationResponse getAuthorization() {
		return get( AUTHORIZATION );
	}

	public List<Album> getAlbums() {
		return get( ALBUM );
	}

	public Album getAlbum() {
		return get( ALBUM_DETAILS );
	}

	public Artist getArtist() {
		return get( ARTIST );
	}

	public List<Artist> getArtistsDetails() {
		return get( ARTISTS_DETAILS );
	}

	public Response getAuthorizationResponse() {
		return get( AUTHORIZATION_RESPONSE );
	}

	public Response getAlbumResponse() {
		return get( ALBUM_RESPONSE );
	}

	public Response getArtistResponse() {
		return get( ARTIST_RESPONSE );
	}

	public AuthorizationFixture switchToAuthorization() {
		return instantiate( AuthorizationFixture.class );
	}

	public AlbumsFixture switchToAlbums() {
		return instantiate( AlbumsFixture.class );
	}

	public ArtistsFixture switchToArtists() {
		return instantiate( ArtistsFixture.class );
	}

}
