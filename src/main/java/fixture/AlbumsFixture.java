package fixture;

import org.apache.http.HttpStatus;

import io.restassured.response.Response;
import java.util.ArrayList;
import java.util.List;
import model.albums.Album;
import utils.Keys;

public class AlbumsFixture extends SpotifyBaseFixture {

	public AlbumsFixture getAlbumsDetails() {
		List<Album> albums = getAlbums();
		if ( albums == null ) {
			albums = new ArrayList<>();
		}

		List<String> albumsIds = new ArrayList<>();
		albumsIds.add( "2uEMUGs6oc41FyMFH9Jzjb" );
		albumsIds.add( "3SpBlxme9WbeQdI9kx7KAV" );

		Response response = albumsClient.getAlbums( albumsIds, "RO", getAuthorization().getAccess_token() );

		response.then().statusCode( HttpStatus.SC_OK );
		albums = response.then().extract().body().jsonPath().getList( "albums", Album.class );

		store( Keys.ALBUM_RESPONSE, response );
		store( Keys.ALBUM, albums );
		return this;
	}

	public AlbumsFixture getAlbumsWithoutIds() {
		List<Album> albums = getAlbums();
		if ( albums == null ) {
			albums = new ArrayList<>();
		}

		Response response = albumsClient.getAlbumsWithoutIdParameter( "RO", getAuthorization().getAccess_token() );
		albums = response.then().extract().body().jsonPath().getList( "albums", Album.class );

		store( Keys.ALBUM_RESPONSE, response );
		store( Keys.ALBUM, albums );
		return this;
	}

	public AlbumsFixture getAlbumsWithInvalidId() {
		List<Album> albums = getAlbums();
		if ( albums == null ) {
			albums = new ArrayList<>();
		}

		List<String> albumsIds = new ArrayList<>();
		albumsIds.add( "invalidId" );
		Response response = albumsClient.getAlbums( albumsIds, "RO", getAuthorization().getAccess_token() );
		albums = response.then().extract().body().jsonPath().getList( "albums", Album.class );

		store( Keys.ALBUM_RESPONSE, response );
		store( Keys.ALBUM, albums );

		return this;
	}

	public AlbumsFixture getAlbumDetails( String albumId ) {
		Album albumDetails = getAlbum();
		if ( albumDetails == null ) {
			albumDetails = new Album();
		}

		Response response = albumsClient.getAlbumDetails( albumId, "RO", getAuthorization().getAccess_token() );
		albumDetails = response.getBody().as( Album.class );

		store( Keys.ALBUM_RESPONSE, response );
		store( Keys.ALBUM_DETAILS, albumDetails );
		return this;
	}

}
