package fixture;

import io.restassured.response.Response;
import model.artists.Artist;
import utils.Keys;

public class ArtistsFixture extends SpotifyBaseFixture {

	public ArtistsFixture getArtistDetails( String artistId ) {
		Artist artist = getArtist();
		if ( artist == null ) {
			artist = new Artist();
		}

		Response response = artistsClient.getArtistDetails( artistId, getAuthorization().getAccess_token() );
		artist = response.getBody().as( Artist.class );

		store( Keys.ARTIST_RESPONSE, response );
		store( Keys.ARTIST, artist );
		return this;
	}
}
