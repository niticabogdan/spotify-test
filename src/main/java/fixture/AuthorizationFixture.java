package fixture;

import org.apache.http.HttpStatus;

import io.restassured.response.Response;
import model.authorization.AuthorizationResponse;
import model.authorization.GrantType;
import utils.Keys;

public class AuthorizationFixture extends SpotifyBaseFixture {

	public AuthorizationFixture getAuthorizationToken() {
		AuthorizationResponse authorizationResponse = getAuthorization();
		if ( authorizationResponse == null ) {
			authorizationResponse = new AuthorizationResponse();
		}
		Response response = authorizationClient.generateAuthorizationToken( GrantType.CLIENT_CREDENTIALS.getValue() );
		response.then().statusCode( HttpStatus.SC_OK );
		authorizationResponse = response.getBody().as( AuthorizationResponse.class );

		store( Keys.AUTHORIZATION, authorizationResponse );

		return this;
	}

	public AuthorizationFixture getAuthorizationTokenWithoutEmptyGrantType() {
		AuthorizationResponse authorizationResponse = getAuthorization();
		if ( authorizationResponse == null ) {
			authorizationResponse = new AuthorizationResponse();
		}
		Response response = authorizationClient.generateAuthorizationToken( " " );

		store( Keys.AUTHORIZATION_RESPONSE, response );

		return this;
	}

	public AuthorizationFixture getAuthorizationTokenWithWrongGrantType( String grantType ) {
		AuthorizationResponse authorizationResponse = getAuthorization();
		if ( authorizationResponse == null ) {
			authorizationResponse = new AuthorizationResponse();
		}
		Response response = authorizationClient.generateAuthorizationToken( grantType );

		store( Keys.AUTHORIZATION_RESPONSE, response );

		return this;
	}

}
