package steps;

import client.AuthorizationClient;
import fixture.SpotifyBaseFixture;

public class BaseStep {

	protected AuthorizationClient authorizationClient = new AuthorizationClient();

	protected SpotifyBaseFixture fixture = new SpotifyBaseFixture();
}
