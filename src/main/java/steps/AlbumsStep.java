package steps;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.is;

import org.testng.Assert;

import io.cucumber.java.en.And;
import io.restassured.response.Response;
import java.util.List;
import model.albums.Album;

public class AlbumsStep extends BaseStep {

	@And( "Get Albums Details" )
	public void get_albums_details() {

		Response response = fixture.switchToAuthorization()
				.getAuthorizationToken()
				.switchToAlbums()
				.getAlbumsDetails()
				.getAlbumResponse();

		List<Album> responseAlbums = fixture.getAlbums();

		response.then().statusCode( SC_OK );

		Assert.assertEquals( responseAlbums.size(), 2 );
	}

	@And( "Get Albums Details Without Album IDs" )
	public void get_albums_details_without_ids() {

		Response response = fixture.switchToAuthorization()
				.getAuthorizationToken()
				.switchToAlbums()
				.getAlbumsWithoutIds()
				.getAlbumResponse();

		response.then().statusCode( SC_BAD_REQUEST )
				.body( "error.message", is( "invalid id" ) )
				.body( "error.status", is( SC_BAD_REQUEST ) );
	}

	@And( "Get Albums Details With Invalid Album Id" )
	public void get_album_details_with_invalid_album_id() {

		Response response = fixture.switchToAuthorization()
				.getAuthorizationToken()
				.switchToAlbums()
				.getAlbumsWithInvalidId()
				.getAlbumResponse();

		List<Album> responseAlbums = fixture.getAlbums();
		response.then().statusCode( SC_OK );
	}

	@And( "Get Details Of One Album" )
	public void get_details_one_album() {

		Response response = fixture.switchToAuthorization()
				.getAuthorizationToken()
				.switchToAlbums()
				.getAlbumDetails( "3SpBlxme9WbeQdI9kx7KAV" )
				.getAlbumResponse();

		Album responseAlbum = fixture.getAlbum();

		response.then().statusCode( SC_OK );
		Assert.assertEquals( "3SpBlxme9WbeQdI9kx7KAV", responseAlbum.id );
	}
}
