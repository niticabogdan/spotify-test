package steps;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.restassured.response.Response;

public class AuthenticationStep extends BaseStep {

	@Given( "Token is not generated if body is empty" )
	public void token_not_generated_empty_body() {

		Response response = fixture.switchToAuthorization()
				.getAuthorizationTokenWithoutEmptyGrantType()
				.getAuthorizationResponse();

		response.then().statusCode( SC_BAD_REQUEST );
	}

	@And( "Token is not generated with wrong grant type" )
	public void token_not_generated_wrong_grant_type() {

		Response response = fixture.switchToAuthorization()
				.getAuthorizationTokenWithWrongGrantType( "wrong_value" )
				.getAuthorizationResponse();

		response.then().statusCode( SC_BAD_REQUEST );
	}
}
