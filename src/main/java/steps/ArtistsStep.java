package steps;

import static org.apache.http.HttpStatus.SC_OK;

import org.testng.Assert;

import io.cucumber.java.en.And;
import io.restassured.response.Response;
import model.artists.Artist;

public class ArtistsStep extends BaseStep {

	@And( "Get Details on one Artist" )
	public void get_artist_details() {

		Response response = fixture.switchToAuthorization()
				.getAuthorizationToken()
				.switchToArtists()
				.getArtistDetails( "3jK9MiCrA42lLAdMGUZpwa" )
				.getArtistResponse();

		Artist responseArtist = fixture.getArtist();

		response.then().statusCode( SC_OK );

		Assert.assertEquals( responseArtist.getName(), "Anderson .Paak" );
		Assert.assertEquals( responseArtist.getId(), "3jK9MiCrA42lLAdMGUZpwa" );
	}
}
