package client;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.List;

public class AlbumsClient extends BaseClient {

	public Response getAlbums( List<String> ids, String countryCode, String authenticationToken ) {
		RequestSpecification request = getBasePath()
				.header( "Host", "api.spotify.com" )
				.header( "Authorization", "Bearer " + authenticationToken )
				.param( "ids", String.join( ",", ids ) )
				.param( "market", countryCode );

		return request.get( "/albums" );
	}

	public Response getAlbumsWithoutIdParameter( String countryCode, String authenticationToken ) {
		RequestSpecification request = getBasePath()
				.header( "Host", "api.spotify.com" )
				.header( "Authorization", "Bearer " + authenticationToken )
				.param( "market", countryCode );

		return request.get( "/albums" );
	}

	public Response getAlbumDetails( String albumId, String countryCode, String authenticationToken ) {
		RequestSpecification request = getBasePath()
				.header( "Host", "api.spotify.com" )
				.header( "Authorization", "Bearer " + authenticationToken );

		return request.get( "/albums/" + albumId );
	}

}
