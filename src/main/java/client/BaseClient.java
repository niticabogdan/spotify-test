package client;

import static io.restassured.RestAssured.given;

import io.restassured.specification.RequestSpecification;
import utils.EnvironmentReader;

public class BaseClient {

	public RequestSpecification getBasePath() {
		RequestSpecification request = given().relaxedHTTPSValidation();
		request.baseUri( EnvironmentReader.getBaseUri() );

		return request;
	}

}
