package client;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utils.EnvironmentReader;

public class AuthorizationClient extends BaseClient {

	public Response generateAuthorizationToken( String grantType ) {
		RequestSpecification request = getBasePath()
				.contentType( ContentType.URLENC )
				.baseUri( EnvironmentReader.getAuthBaseUri() )
				.body( "grant_type=" + grantType )
				.header( "Host", "accounts.spotify.com" )
				.header( "Authorization", "Basic " + EnvironmentReader.getAuthenticationToken() );

		return request.post( "/token" );
	}

}
