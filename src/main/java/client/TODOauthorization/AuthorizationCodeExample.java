package client.TODOauthorization;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.SpotifyHttpManager;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeUriRequest;

import java.net.URI;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

public class AuthorizationCodeExample {

	private static final String clientId = "f2d5168225634426841f8a0743a65393";
	private static final String clientSecret = "168d747960674ae289a2f00402d86272";
	private static final URI redirectUri = SpotifyHttpManager.makeUri( "https://example.com/spotify-redirect" );
	private static final String code = "";
	private static final String codeChallenge = "w6iZIj99vHGtEx_NVl9u3sthTN646vvkiP8OMCGfPmo";

	private static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
			.setClientId( clientId )
			.setClientSecret( clientSecret )
			.setRedirectUri( redirectUri )
			.build();
	private static final AuthorizationCodeUriRequest authorizationCodeUriRequest = spotifyApi.authorizationCodePKCEUri( codeChallenge )
			.build();

	public static void authorizationCodeUri_Sync() {
		final URI uri = authorizationCodeUriRequest.execute();

		System.out.println( "URI: " + uri.toString() );
	}

	public static void authorizationCodeUri_Async() {
		try {
			final CompletableFuture<URI> uriFuture = authorizationCodeUriRequest.executeAsync();

			// Thread free to do other tasks...

			// Example Only. Never block in production code.
			final URI uri = uriFuture.join();

			System.out.println( "URI: " + uri.toString() );
		} catch ( CompletionException e ) {
			System.out.println( "Error: " + e.getCause().getMessage() );
		} catch ( CancellationException e ) {
			System.out.println( "Async operation cancelled." );
		}
	}

//	public static void main( String[] args ) {
//		authorizationCodeUri_Sync();
//		authorizationCodeUri_Async();
//	}
}
