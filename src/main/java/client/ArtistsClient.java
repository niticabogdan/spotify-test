package client;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ArtistsClient extends BaseClient {

	public Response getArtistDetails( String artistId, String authenticationToken ) {
		RequestSpecification request = getBasePath()
				.header( "Host", "api.spotify.com" )
				.header( "Authorization", "Bearer " + authenticationToken );

		return request.get( "/artists/" + artistId );
	}

}
