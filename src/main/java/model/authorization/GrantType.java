package model.authorization;

public enum GrantType {
	CLIENT_CREDENTIALS( "client_credentials" );

	private final String value;

	GrantType( String value ) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
