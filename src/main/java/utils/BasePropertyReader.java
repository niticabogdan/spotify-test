package utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

public class BasePropertyReader {

	private static Properties prop = new Properties();

	protected static void loadProperties( String fileName ) {
		try {
			InputStream resource = BasePropertyReader.class.getClassLoader().getResourceAsStream( fileName );
			if ( resource != null ) {
				prop.load( resource );
			} else {
				throw new RuntimeException( "Failed to load " + fileName + " file!" );
			}
		} catch ( IOException e ) {
			throw new RuntimeException( "Failed to load " + fileName + " file!", e );
		}
	}

	protected static void loadMultipleProperties( final String fileName ) {
		try {
			final Enumeration<URL> systemResources = BasePropertyReader.class.getClassLoader().getResources( fileName );

			while ( systemResources.hasMoreElements() ) {
				prop.load( systemResources.nextElement().openStream() );
			}
		} catch ( IOException e ) {
			throw new RuntimeException( "Failed to load " + fileName + " file!", e );
		}
	}

	/**
	 * Checks for existing system property value first, if empty or null reads from properties file.
	 * If property value contains a variable (e.g. app.uri=http://ipAddress:port/{variable}/serviceName ), variable value will be taken from system property or properties file
	 * and replaced in property value.
	 *
	 * @param property Name of the property
	 * @return String value found
	 */
	protected static String getProperty( String property ) {

		String returnProperty = !isNullOrEmpty( System.getProperty( property ) ) ?
				System.getProperty( property ) : prop.getProperty( property );
		return returnProperty;
	}

	/**
	 * Checks for existing system environment variable value first, if empty or null reads from properties file.
	 * If property value contains a variable (e.g. app.uri=http://ipAddress:port/{variable}/serviceName ), variable value will be taken from system property or properties file
	 * and replaced in property value.
	 *
	 * @param property Name of the property in properties file
	 * @return String value found
	 */
	protected static String getEnvProperty( String property ) {
		String returnProperty = !isNullOrEmpty( System.getenv( property ) ) ?
				System.getenv( property ) : prop.getProperty( property );
		return returnProperty;
	}

	/**
	 * Checks for existing system environment variable value first, if empty or null reads from properties file.
	 *
	 * @param envProperty Name of the specified environment variable
	 * @param property    Name of the property in properties file
	 * @return String value found
	 */
	protected static String getEnvProperty( String envProperty, String property ) {
		String returnProperty = !isNullOrEmpty( System.getenv( envProperty ) ) ?
				System.getenv( envProperty ) : prop.getProperty( property );
		return returnProperty;
	}

	private static boolean isNullOrEmpty( String str ) {
		return str == null || str.trim().isEmpty();
	}
}
