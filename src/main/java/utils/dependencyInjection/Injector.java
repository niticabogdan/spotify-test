package utils.dependencyInjection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import utils.dependencyInjection.annotation.Inject;
import utils.dependencyInjection.annotation.PageInject;

public class Injector {

	private static Injector injector;

	public synchronized static Injector getInstance() {
		if ( injector == null ) {
			injector = new Injector();
		}
		return injector;
	}

	public static <T> T instantiate( Class<T> clazz ) {
		try {
			T instance = clazz.getConstructor().newInstance();
			getInstance().injectObjects( instance );
			return instance;
		} catch ( InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e ) {
			throw new RuntimeException( "Can't instantiate class " + clazz.getName(), e );
		}
	}

	public void injectObjects( Object instance ) throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {

		List<Field> declaredFields = new ArrayList<>();
		Class clazz = instance.getClass();
		do {
			declaredFields.addAll( Arrays.asList( clazz.getDeclaredFields() ) );
		} while ( ( clazz = clazz.getSuperclass() ) != null );

		for ( Field field : declaredFields ) {
			if ( field.isAnnotationPresent( Inject.class ) ) {
				inject( instance, field );
			}
		}
	}

	public void injectPage( Object instance, Object... arguments ) throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {

		List<Field> declaredFields = new ArrayList<>();
		Class clazz = instance.getClass();
		do {
			declaredFields.addAll( Arrays.asList( clazz.getDeclaredFields() ) );
		} while ( ( clazz = clazz.getSuperclass() ) != null );

		for ( Field field : declaredFields ) {
			if ( field.isAnnotationPresent( PageInject.class ) ) {
				inject( instance, field, arguments );
			}
		}
	}

	private void inject( Object classInstance, Field field, Object... arguments ) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		field.setAccessible( true );
		Object fieldInstance;
		Constructor contructor = field.getType().getDeclaredConstructors()[0];
		if ( contructor.getParameterCount() == 0 ) {
			fieldInstance = contructor.newInstance();
		} else if ( contructor.getParameterCount() == arguments.length ) {
			fieldInstance = contructor.newInstance( arguments );
		} else {
			throw new RuntimeException( "Can't instantiate " + field.getName() + "! No valid constructor found" );
		}
		injectObjects( fieldInstance );
		field.set( classInstance, fieldInstance );
	}
}
