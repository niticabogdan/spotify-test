package utils;

public class EnvironmentReader extends BasePropertyReader {

	static {
		String envFile = System.getProperty( "env" );
		if ( envFile == null ) {
			envFile = "qa";
		}
		String filePath = "environments/".concat( envFile.concat( ".properties" ) );
		loadProperties( filePath );
	}

	public static String getBaseUri() {
		return getProperty( "spotify.api.baseUri" );
	}

	public static String getAuthBaseUri() {
		return getProperty( "spotify.api.auth.baseUri" );
	}


	public static String getAuthenticationToken() {
		return getProperty( "spotify.api.authentication" );
	}

}
