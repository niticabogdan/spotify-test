package utils.store;

public class StoreAwareContext {

	private StoreManager storeManager = StoreManager.getStoreManager();

	public StoreManager getStoreManager() {
		return storeManager;
	}

	public <T> T get( SpotifyKeys key ) {
		return storeManager.get( key );
	}

	public void store( SpotifyKeys key, Object value ) {
		storeManager.store( key, value );
	}

	public void incrementKey( SpotifyKeys key ) {
		Number value = storeManager.get( key );

		if ( value instanceof Integer ) {
			value = value.intValue() + 1;
		}
		if ( value instanceof Double ) {
			value = value.doubleValue() + 1.0;
		}
		if ( value instanceof Float ) {
			value = value.floatValue() + 1F;
		}
		if ( value instanceof Long ) {
			value = value.longValue() + 1L;
		}
		if ( value instanceof Short ) {
			value = value.shortValue() + 1;
		}
		if ( value instanceof Byte ) {
			value = value.byteValue() + 1;
		}

		storeManager.store( key, value );
	}
}
