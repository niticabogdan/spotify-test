package utils.store;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class StoreManager {

	private static StoreManager storeManager = new StoreManager();

	public static StoreManager getStoreManager() {
		return storeManager;
	}

	private Map<String, Object> store = new ConcurrentHashMap<>();

	public void store( SpotifyKeys key, Object value ) {
		if ( value == null ) {
			store.remove( getThreadSafeKey( key ) );
		} else {
            if (store.get(getThreadSafeKey(key)) != null) {
                store.remove(getThreadSafeKey(key));
            }
			store.put( getThreadSafeKey( key ), value );
		}
	}

	public <T> T get( SpotifyKeys key ) {
        return (T) store.get(getThreadSafeKey(key));
	}

	public synchronized void reset() {
		Set<String> strings = store.keySet();
		for ( String key : strings ) {
			if ( key.endsWith( "-" + Thread.currentThread().getId() ) ) {
				store.remove( key );
			}
		}
	}

	private String getThreadSafeKey( SpotifyKeys key ) {
		return key + "-" + Thread.currentThread().getId();
	}

}
