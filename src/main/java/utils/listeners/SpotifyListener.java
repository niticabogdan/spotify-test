package utils.listeners;

import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.lang.reflect.InvocationTargetException;
import utils.dependencyInjection.Injector;
import utils.store.StoreManager;

public class SpotifyListener extends TestListenerAdapter {

	private StoreManager storeManager = StoreManager.getStoreManager();

	@Override
	public void onStart( ITestContext testContext ) {
		try {
			ITestNGMethod[] allTestMethods = testContext.getAllTestMethods();
			for ( ITestNGMethod method : allTestMethods ) {
				Injector.getInstance().injectObjects( method.getInstance() );
			}
		} catch ( InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e ) {
			throw new RuntimeException( "Can't instantiate test method ", e );
		}
	}

	@Override
	public void onTestStart( ITestResult result ) {
		try {
			Object instance = result.getMethod().getInstance();
			Injector.getInstance().injectObjects( instance );
		} catch ( InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e ) {
			throw new RuntimeException( "Can't instantiate test method ", e );
		}
	}

	@Override
	public void onTestSuccess( ITestResult tr ) {
		storeManager.reset();
	}

	@Override
	public void onTestFailure( ITestResult tr ) {
		storeManager.reset();
	}

	@Override
	public void onTestSkipped( ITestResult tr ) {
		storeManager.reset();
	}
}
