package utils;

import utils.store.SpotifyKeys;

public enum Keys implements SpotifyKeys {
	AUTHORIZATION,
	AUTHORIZATION_RESPONSE,
	ALBUM_RESPONSE,
	ALBUM,
	ALBUM_DETAILS,
	ARTIST,
	ARTIST_RESPONSE,
	ARTISTS_DETAILS;
}
