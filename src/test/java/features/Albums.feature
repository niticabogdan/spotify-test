Feature: Album Details

  Scenario: Get Albums Details
    And Get Albums Details

  Scenario: Get Albums Details Without Ids
    And Get Albums Details Without Album IDs

  Scenario: Get Albums Details Without Ids
    And Get Albums Details With Invalid Album Id

  Scenario: Get Details Of One Album
    And Get Details Of One Album