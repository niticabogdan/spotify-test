package runner;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import utils.listeners.SpotifyListener;

@CucumberOptions(
		features = "src/test/java/features",
		plugin = { "json:target/cucumber.json",
				"junit:target/testresult.xml",
				"html:target/environment/cucumber-pretty",
				"pretty" },
		glue = "steps",
		monochrome = true )
@Listeners( { SpotifyListener.class } )
public class SpotifyTestRunner extends AbstractTestNGCucumberTests {

	@Override
	@DataProvider( parallel = true )
	public Object[][] scenarios() {
		return super.scenarios();
	}
}
